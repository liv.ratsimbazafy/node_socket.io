## MY_IRC
A basic Realtime Chat application with NodeJs and Socket.io

## Requirements
node -v > 10.0

## Installation
- Clone the repository

## Usage
- Run
```bash
npm start
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Version
MVP

## License 
MIT