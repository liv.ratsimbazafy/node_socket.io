const path = require("path");
const express = require("express");
const http = require("http");
const socketIo = require("socket.io");
const formatMessage = require("./utils/messages");
const {
    userJoin,
    getCurrentUser,
    userLeave,
    getRoomUsers,
} = require("./utils/users");

const app = express();
const server = http.createServer(app);
const io = socketIo(server);

app.use(express.static(path.join(__dirname, "public")));

const serverName = "SOCKET";

// Listen for ws connection
io.on("connection", (socket) => {
    socket.on("joinRoom", ({ username, room }) => {
        const user = userJoin(socket.id, username, room);
        socket.join(user.room);

        // Emit to a single connected client
        socket.emit(
            "message",
            formatMessage(serverName, `Welcome to ${serverName} ${username}`)
        );

        // Emit to all connected client except emiter
        socket.broadcast
            .to(user.room)
            .emit(
                "message",
                formatMessage(
                    serverName,
                    `${username} has joined the ${room} chat room`
                )
            );

        // send users and room info
        io.to(user.room).emit("roomUsers", {
            room: user.room,
            users: getRoomUsers(user.room),
        });
    });

    socket.on("chatMessage", (msg) => {
        const user = getCurrentUser(socket.id);
        io.to(user.room).emit("message", formatMessage(user.username, msg));
    });

    // Listen when client disconnect
    socket.on("disconnect", () => {
        const user = userLeave(socket.id);

        if (user) {
            io.to(user.room).emit(
                "message",
                formatMessage(serverName, `${user.username} has left the Chat`)
            );
        }

        // send users and room info
        io.to(user.room).emit("roomUsers", {
            room: user.room,
            users: getRoomUsers(user.room),
        });
    });
});

const PORT = process.env.PORT || 3000;
server.listen(PORT, () => console.log(`Server is up on port ${PORT}`));
