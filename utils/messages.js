const moment = require("moment");

/**
 * @description Beautify chat message
 * @param {*} username 
 * @param {*} message 
 * @returns 
 */
function formatMessage(username, message) {
    return {
        username,
        message,
        time: moment().format("h:mm a"),
    };
}

module.exports = formatMessage;
