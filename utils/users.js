const users = [];

/**
 *@description Handle user who joined the chat
 * @param {*} id
 * @param {*} username
 * @param {*} room
 * @returns
 */
const userJoin = (id, username, room) => {
    const user = { id, username, room };
    users.push(user);
    return user;
};

/**
 *@description Get the connected user
 * @param {*} id
 * @returns
 */
const getCurrentUser = (id) => {
    return users.find((user) => user.id === id);
};

const userLeave = (id) => {
    const index = users.findIndex((user) => user.id === id);
    if (index !== -1) return users.splice(index, 1)[0];
};

/**
 *@description Get users present in the given chat room
 * @param {*} room
 * @returns
 */
const getRoomUsers = (room) => {
    return users.filter((user) => user.room === room);
};

module.exports = {
    userJoin,
    getCurrentUser,
    userLeave,
    getRoomUsers,
};
